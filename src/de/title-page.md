# Das Dense Component Entity System

[<img src="img/dces_space.png" width="720"/>](img/dces_space.png)

*von Florian Blasius, mit der Unterstützung der Rust Community*<br>
*kommentiert und dokumentiert von Ralf Zerres und anderen Beitragenden *

Die Textversion geht davon aus, dass Du Rust in der version 1.60.0
oder aktueller verwendest.
*Cargo.toml* sollte eine Definition mit `edition="2021"` enthalten. Die erlaubt Ausdrücke der
Rust 2021 Edition in allen abgeleiteten Projektdateien zu verwenden.

Die vorliegende 2020 Ausgabe dieses Leitfadens ist die
Erstveröffentlichung. Sie bezieht sich auf die DCES Version 0.4.0.

- Anhang A Schlüsslworte, beschreibt die eingeführten generischen Begriffe.
- Anhang C Übersetzungen, sind in Arbeit befindliche
  Dokumente. Weitere Übersetzungen dieses Leitfadens in andere
  Sprachen werden online eingestellt, sobald sie fertig gestellt wurden.

Der Leitfaden wir in HTML gerenderten Versionen über den Link [DCES
guide][dces_guide_en] für das online lesen bereitgestellt. Um sie auch
offline lesen zu können kannst Du alternativ eine `pdf` oder `ebook`
Version, oder die Quellen selbst herunterladen. Die Quell-Texte kannst
Du anschließend mit dem Programm `mdbook` in dein präferiertes Format rendern.

Wenn sich die weitere Entwicklung stabilisiert hat, wir dieser
Leitfaden in mehreren Sprachen veröffentlicht. Die Struktur der
Quell-Verzeichnis ist bereits so aufgebaut, das [lokalisierte
Unterverzeichnisse][mdbook_localization] die übersetzten Texte
enthalten. Daher benötigst Du eine erweiterte Version von mdbook, um
diese in das Zielformat zu übertragen. [Ruin0x11][mdbook_branch]
betreut eine git branch **localization**. Ein [PR][mdbook_pr_1306] mit
den nötigen Änderungen ist bereits in der upstream branch eingereicht.

[mdbook_localization]: https://github.com/Ruin0x11/mdBook/tree/localization
[mdbook_branch]: https://github.com/Ruin0x11/mdBook.git
[mdbook_pr_1306]: https://github.com/rust-lang/mdBook/pull/1306
[dces_guide_en]:  https://gitlab.redox-os.org/redox-os/dces-rust/wiki/guide-en/index.html

Start die Intallation von dieser mdBook Version wie folgt:

```console
TMPDIR=<your_temporary_directory>
mkdir -p $TMPDIR; cd $TMPDIR
git clone https://github.com/Ruin0x11/mdBook.git
cd mdBook
git checkout localization
cargo update
cargo install --path .
```

Wir unterstützen auch die Visualisierung von Prozessen, was auf den
Funktionen von [mermaid][mermaid_home] aufbaut. Um mermaid
herunterzuladen und aus den Quellen zu übersetzen verwendest Du bitte
folgende Befehle:

[mermaid_home]: https://mermaid-js.github.io/mermaid/#

```console
cargo install mdbook-mermaid
mermaid install
```

Nun, da alle benötigten Programme auf deinem Entwicklungssystem installiert sind kannst du den eigentlichen Leitfaden wie folgt erzeugen:

```console
cd  <the_DCES_guide__directory>
mdbook build --language de --dest-dir dces-guide/de --open
```
