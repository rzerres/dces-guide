# DCES Beispiel - Shared

Dieser Abschnitt dokumentiert die Beispielanwendung **dces_shared**.


In einem minimale Anwendungsumfang erstellt das Beispiel eine
**world**. Hier werden zwei **entities** erzeugt. Beiden werden
**components** zugewiesen. Die Komponenten stellen die Eigenschaft
"name", "depth" und "size" bereit.  Bei der Erstellung der zweiten
Eigenschaft (component "name" =="CheckBox") mit dem
**ComponentBuilder** verwenden wir die Methode "with_shared()". Diese
verwendet eine Referenz auf die bereits existierende Komponente
"size".

Ein **size system** ermöglicht es, die Werte für jede Teilkomponente
("width", "height") in der Struktur "size" zu erhöhen.

Ein **print system** steuert eine Abfrage in den
Eigenschaften-Speicher "e_store", wählt die gewünschten Eigenschaften
und druckt die "Werte" der Bestandteile mit dem Schlüsselwert
"name". Die verwendete Logik ist simpel: Gib die Werte für die
Eigenschaft "name", "depth" und die Komponentenwerte für "width" und
"height" der Eigenschaft "size" auf der Konsole aus (stdout).

## Überarbeitung Cargo.toml

Werfe zunächst einen Blick auf die zugehörige *Cargo.toml* Datei, die
in Listing 1-1 ausgebeben wird.

```toml,ignore
{{#include ./listings/ch09-04-dces-shared/listing-04-01/Cargo.toml}}
```

<span class="caption">Listing 1-1: Projekt Metadata "dces_shared"</span>

## Der Program-Quellcode

Der gesamte **DCES** spezifische Quellcode der erforderlich ist um
unser Beispiel **dces_shared** zu erzeugen, findest du im
[Listing 1-2][dces_shared], das in der Datei *src/main.rs* kodiert wird.

Gib den folgenden Befehl ein, um den Kopilierungslauf zu starten und
anschließend die erzeugte App im debug Modus zu starten:

```console
$ cargo run --example shared
```


Nachfolgende Ausgabe sollte im Konsolen-Fenster ausgegeben werden:

```console
entity: 0; name: Button; width: 6; height: 6
entity: 1; name: CheckBox; width: 6; height: 6
```

[dces_shared]: #complete-example-source

## Zusammenfassung und Anmerkungen

### Die Anatomie der `dces_minimal` Anwendung

Lass uns die Details durchsehen, die innerhalb unserer
**dces_shared** App umgesetzt wurden.  Eine tiefergehende Darstellung
einer typischen `DCES` Anwendung findest Du innerhalb der annotierten
Quelle [dces_basic][dces_basic_source].

[dces_basic_source]: ./ch09-01-dces-basic.md

Der foldene Quellcode Block schneidet die Definition des **PrintSystems** heraus:

```console
{{#include ./listings/ch09-04-dces-shared/listing-04-01/src/main.rs:Implement_PrintSystem}}
```

Mit einer Schleife verarbeiten wir die **entities** des relevanten
"e_store" und iterieren über alle Komponenten, deren Schlüssel
 `"name"` entsprechen.

Der uns interessierende neue Teil liegt in der Verarbeitung der
Size-Struktur und der Erstellung der zweiten Entität. In der *main*
Funktion, erzeugen wir die zweite Entität mit
**ComponentBuilder**. Wenn wir die Komponente "size" erzeugen,
verwenden wir die Methode `with_shared`. Diese Methode kann auf die
bereits bestehende Sturktur "size" zurückgreifen (borrow). Sie
verwendet damit die bereits zugewiesenen Werte.

```console
{{#include ./listings/ch09-04-dces-shared/listing-04-01/src/main.rs:ComponentShared}}
```

### Vollständiger Quellcode des Beispiels

Im Listing wird der vollständige Quellcode des **dces_shared** Beispiels aufgeführt.

```rust,ignore
{{#rustdoc_include ./listings/ch09-04-dces-shared/listing-04-01/src/main.rs:All}}
```

<span class="caption">Listing 1-2: dces_shared - Erstellung einer World, ihrer Enitäten und einer shared Komponente.</span>

### Kompilierung und Aufruf im zwei Schritten

Die mit **cargo** übersetzte App `dces_shared` wird im
Unterverzeichnis `target` des Projekt-Wurzelverzeichnis abgespeichert.

```console
$ cargo build --release --bin dces_shared.rs
$ ../target/release/dces_shared
```

Auf Windows Systemen musst Du `backslash` als Pfad Trennzeichen nutzen:

```powershell
> cargo build --release --bin dces_shared.rs
> ..\target\release\dces_shared.exe
```
