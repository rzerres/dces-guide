# DCES Beispiel - Basic

Dieser Abschnitt dokumentiert die Beispeilanwendung **dces_basic**.

Im Beispiel-Quellcode wird nur eine **world** Instanz erzeugt. Es
werden drei **entities**, jede mit einer zugewiesenen **component**,
welche die Eigenschaften "name", "depth" und "size" bereitstellen.

Ein **size system** steuert eine Abfrage über die Eigenschaften und
verändert die Werte für die Struktur "size", mit deren Bestandteilen
*width* und *height*. Die angewendete Logik ist rudimentäre: Erhöhe
den Werte um eins.

Ein **print system** steuert eine Abfrage in den
Eigenschaften-Speicher "e_store", wählt die gewünschten Eigenschaften
und druckt die "Werte" der Bestandteile mit dem Schlüsselwert
"name". Wiederum ist die Logik einfach gehalten: Gib die
Eigenschafts-Namen, neben den Werten für *width* und *height* auf der
Konsole aus (stdout).

##  Wurzelverzeichnis des Projekts

Bitte wechsel zunächst in das Wurzelverzeichnis deines Projekts. Hast
die angesprochene App noch nicht erstelle, tippe bitte folgenden
Befehl in der Konsole ein:

```console
$ cd ~/guide/examples
```

##  Erstellung des Quellcodes

Wir verwenden nun *cargo*, um die Struktur für die App zu erzeugen.
Alle Template-Aufgaben werden durch die von *caro* bereitgestellen
Vorlagen-Behandlungen abgearbeitet.

```console
$ cargo new dces_basic
$ cd dces_basic
```

Das erste Befehl `cargo new`, bestimmt den Namen des Projekts
("`dces_basic`"). Der zweite Befehl wechselt in das neue Projektverzeichnis.

Schauen wir uns die erzeugte Datei *Cargo.toml* an:

```toml
{{#include ./listings/ch09-01-dces-basic/no-listing-01-01-cargo-new/Cargo.toml}}
```

<span class="caption">Listing 1-1: Standard Metadaten "dces_basic"</span>

Mit `cargo new` wurde eine Standardstruktur für das Projekt
erstellt. Vielleicht sind bereits die Informationen für den Autor
ausgetauscht, sofern *Cargo* die Werte aus Deinen
Umgebungsvariablen, bzw. einer globalen Konfigurationsdatei auslesen konnte.

 *Cargo* erstelle darüber hinaus auch den Quellcode für ein "Hello, world!"
Programm. Lass uns daher die erzeugte Datei *src/main.rs* überprüfen:

```rust
{{#rustdoc_include ./listings/ch09-01-dces-basic/no-listing-01-01-cargo-new/src/main.rs}}
```

<span class="caption">Listing 1-2: Standardstruktur mit "main.rs"</span>

Es besteht kein Grund, diesen Stand mit einem Aufruf von `cargo run`
in eine Binardatei zu wandeln. Wir machen sofort weiter und
überarbeiten die Projekt Metadaten und den Programm-Quellcode.

### Überarbeitung Cargo.toml

Zunächst öffnest Du bitte die *Cargo.toml* Datei und veränderst deren
Inhalt mit den Werten aus Listing 1-3.

```toml,ignore
{{#include ./listings/ch09-01-dces-basic/listing-01-01/Cargo.toml:All}}
```

<span class="caption">Listing 1-3: Projekt Metadata "dces_basic"</span>

Vielleicht wunderst Du Dich, warum die Eigenschaft *name* in der *Cargo.toml* Datei als Wert
`dces_basic` formatiert wurde.

```toml,ignore
{{#include ./listings/ch09-01-dces-basic/listing-01-01/Cargo.toml:Name}}
```

Es ist eine gute Angewohnheit, dir Rusts Namenskonvention anzuwenden:
Sie ermutigt Sie sogenannte [snake_case][naming] Formatierung zu
verwenden. Beim Erweitern der **DCES** Beispielquellen werden wir
immer ein Gruppierungspräfix `dces` beibehalten. Auf diese Weise wird
unsere erste Beispiel-App den Namen `dces_basic` erhalten.

[naming]: https://rust-lang.github.io/api-guidelines/naming.html

### Überarbeitung main.rs

Der gesamte **DCES** spezifische Quellcode der erforderlich ist um
unser erstes Beispiel zu erzeugen, findest du im
[Listing1-4][dces_basic], das in der Datei *src/main.rs* kodiert wird.

Speichere deine Änderungen in der Datei und gehe zurück in das
Konsolen-Fenster. Gib den folgenden Befehl ein, um den
Kopilierungslauf zu starten und anschließend die erzeugte App zu
starten.

```console
$ cargo run --example basic
```

Nachfolgende Ausgabe sollte im Konsolen-Fenster ausgegeben werden:

```console
Button width: 6; height: 6
CheckBox width: 4; height: 4
RadioButton width: 5; height: 7
```

[dces_basic]: #complete-example-source

## Zusammenfassung und Anmerkungen

### Die Anatomie einer DCES Anwendung

Lass uns die Details durchsehen, die innerhalb unserer **DCES-basic** umgesetzt wurden.
Wir greifen hierzu nur die relevanten Teile des Puzzles heraus:

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:Use}}
```

Die erste Zeile führt eine *use* Deklaration ein. Dies Deklaration
wird verwendet, um die Pfadangabe zu verkürzen. So können im Quellcode
direkt die referenzierten Module angesprochen werden.
Die Anweisung *prelude* ist ein bequemer Weg zu einer Liste von Dingen, die Rust automatisch
in Ihr Programm importiert. Beispielsweise binden wir hier den Pfad *dces::prelude*. Alle
Standardobjekte, die in diesem Pfad definiert sind (referenziert mit *::*), sind nun
in Deinem Quellcode unter dem Kurznamen zugänglich. Du kannst den Präfix (*dces::prelude::*) auslassen.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:FunctionMain}}
```

Die `main` Funktion hat eine besondere Eigenschaft: Sie definiert den
ersten Ausfühurngspunkt in einer kompilierten Rust Anwendung (In
unserer Betrachtung lassen wir bewußt Rust's minimale **runtime**
aussen vor). Unserer `main` Funktion besitzt keine Parameter und gibt
auch nichts als Ergebnis zurück. Würden wir Parameter verwenden,
würden sie innerhalt der Klammern aufgeführt (`()`).

Bitte beachte ebenso, das  der Funktionsrumpf in geschweiften Klammern
eingebunden  ist  (`{}`).  Rust  erzwingt diese  um  alle  verwendeten
Funktionen. Es ist guter Stil, wenn die eröffnende geschweifte Klammer
auf der  gleichen Zeile wie die Funktionsbeschreibung selbst mit einem Leerzeichen getrennt angefügt
wird.

Ein automatisches Formatierungswerkzeug (`rustfmt`) hilft, Rust's
Standard-Style einzuhalten. DCES folgt diesem Leitfaden. `rustfmt`
wird Deinen Quellcode prüfen und etwaige Abweichungen
aufzeigen. Abhängig von der verwendeten Toolchain ist das Programm
wahrscheinlich schon auf Deinem Entwicklungssystem
installiert. Andernfalls nutze bitte die Online-Dokumentation für
weitere Details.

Lass uns auf ein paar andere wichtige Details in unserem Beispiel konzentrieren:

* Erstens, Dem Rust's Style folgend verwenden wir für das Einrückungen immer vier Leerzeichen (kein *tab*!).
* Zweitens, wir erstellen fünf **entities** innerhalb einer **world**.
* Drittens, wir erstellen zwei **systems** innerhalb der world.
* Viertens, wir starten einen **run loop** innerhalb der world.

Als ein erster Schritt, erzeugen wir eine veränderbare (*mutable*) world Struktur. Diese Struktur vewendet einen **EntityStore**

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:CreateWorld}}
```

Anschließend erstellen wir fünf neue ***entities** innerhalb der
world. Für jede Eigenschaft werden **components** zugewiesen. Hierzu
nutzen wir für die Erstellung der Komponenten die Methode
**ComponentBuilder**. Letztere definiert die Werte (Zeichenkette mit
einem Großbuchstaben). Ihre zugehörigen Schlüsselnamen lauten
**name**, **depth** und **size** (in Klammern). Im abschließenden
Block wird die **build** Methode aufgerufen und die Routine damit
ablauffähig.

Der foldene Quellcode block schneidet die Definition der ersten
Eigenschaft heraus:

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:CreateEntity}}
```
Bitte beachte, das "size" selbst als eine Struktur definiert ist, die ihrerseits die Werte **width** und
**height** gruppiert.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:StructSize}}
```

Wir sollten erwähnen, dass der Beispiel-Code alle **entities** mit der
gleichen Anzahl und mit gleichem Namen definiert werden. Das ist weder
erzwungen noch eine Design-Restriktion. Welche Komponente
(**components**) welcher Eigenschaft (**entity**) zugewiesen wird ist
prinzipiell völlig willkürlich und kann je nach Implementierung
erheblich voneinander abweichen.

Unsere App wird ein **Size-** und ein **Print-System** bereitstellen.
Beide werden die Veränderung von Eigenschaften über den
**EntityComponent Manager (EMS)** verwalten. Der ECM kann
Eigenschaften und Komponenten im jeweiligen **EnitityStore** entleihen
oder verändern (borrow or mutate). Daher ist es möglich eigenständige
und voneinander unabhängige **store structures** zuzuweisen (hier:
e_store, cstore).

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:Implement_SizeSystem}}
```

Nun verarbeiten wir den jeweiligen **store** mit einer einfachen
Schleife. Wir verwenden die **get()** Methode und verwenden die Werte
der jeweiligen Eigenschaften und Komponenten unserer definierten Abfrage.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:Loop_Entities}}
```

### Vollständiger Quellcode des Beispiels

Im Listing wird der vollständige Quellcode des **dces_basic** Beispiels aufgeführt.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:All}}
```

<span class="caption">Listing 1-4: dces_basic - Erstellt ein World, ihre Eigenschaften und Systeme.</span>

### Kompilierung und Aufruf im zwei Schritten

Bevor Du eine DCES Anwendung aufrufen kannst, muss der Quellcode in ein ablauffähiges Programm übersetzt werden.
Ein typisches DCES Projekt wird dieses ablauffähige Programm mit einem Aufruf von **cargo** erzeugen lassen.
**cargo** wird das Ergebnis in einem Unterverzeichnis (target) der Projektwurzel ablegen.

Profile können verwendet werden, die den Kompiler-Anweisungen
gruppieren (z.B. welcher Optimierungs-Grad oder welche debug
Einstellungen). Als Standardwerte wird entweder das `dev` oder das
`test` Profil angewendet. Wird die Kennzeichung ``--release``
angehängt, kommen die `release` oder `bench` Profile zur Anwendung.

```console
$ cargo build --release --bin dces_basic.rs
$ ../target/release/dces_basic
```

Auf Windows Systemen musst Du `backslash` als Pfad Trennzeichen nutzen:

```powershell
> cargo build --release --bin dces_basic.rs
> ..\target\release\dces_basic.exe
```

[troubleshooting]: https://doc.redox-os.org/dces-guide/ch01-01-installation.html#troubleshooting
