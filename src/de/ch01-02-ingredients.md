# Die Bestandteile

`DCES` stellt ein [dense ECS][dense_ecs] basierte Architektur bereit.
Die Interaktion mit `DCES` wird über den ``Entity Component
Manager`(ECM) verwaltet, der als Wrapper-API fungiert.

Im Unterschied zum `Object Oriented Programming` (OOP) Paradigma
separiert DCES Data (data) von
Verhaltensweisen(behavior). **Verhaltensweisen** werden über Systeme
abgebildet. Systeme stellen die Logik bereit und implementieren die
Verhaltensweisen, über die Einheiten mit gegebenen Komponenten
verarbeitet werden. **Daten** sind mit Komponenten verbunden, die dann
Einheiten zugewiesen werden. Daher haben alle Einheiten einen
eindeutigen Identifikator ("id"), über den mehrere dynamisch änderbare
Komponenten zugewiesen sein können. Die Komponenten selbst werden
üblicherweise als Eigenschaften (**properties**) im aufrufenden rust
Quellcode vorgestellt.

[dces]: https://docs.rs/dces
[dense_ecs]: https://gist.github.com/dakom/82551fff5d2b843cbe1601bbaff2acbf

## Die Architekture Ansicht

Um die Architektur eines `ECS` aufzubrechen ist es durchaus hilfreich
zunächst die Kernbestandteile vorzustellen:

* Entity: Ist ein eindeutig identifiziertes Objekt. Die Einheiten
  können keiner oder mehreren Komponenten zugewiesen sein.

* Component: Du kannst Datentypen innerhalb von Komponenten
  abspeichern. Wir sprechen bei den abgespeicherten Werten der
  Komponenten von Eigenschaften ("propertries"). Die Eigenschaft wird
  über ihren Schlüssel identifiziert.

* Entity_Component_Manager: Der ECM verwaltet den sogenannten *entity
store*. Er hat zumeist Bindungen zu dedizierten Eigenschafts- und
assoziierten Komponenten-Speichern).

* Resource: Eine Ressource ist ein Daten-Slot innerhalb einer
**World** Instanz. Daten werden zwischen den einzelnen Systemen
geteilt. Auf Ressourcen kann nur über das für Rust typische *borrow*
Modell (eine schreibender xor mehrere lesende Prozesse) zugegriffen
werden. Über Ressourcen können Komponenten strukturiert werden.

* Store:
Innerhalb von DCES wird für jeden definierten Speicher-Container eine Instanz erzeugt.
Es ist üblich, mehrere spezifische Speicher-Container zu verwenden. Der Speicher-Container
verwaltet mehrere Einheiten, Komponenten und Ressourcen.

* Systems: Die Verhaltensweisen werden über Funktionen
verarbeitet. Diese Verhaltensweisen greifen nach einem *query*
iterativ für alle gefundenen Einheiten und den zugehörigen Komponenten.

* World: Eine World repräsentiert die Wurzel eines DECS Baums. Da es
zulässig ist auch mehrere World-Instanzen zu verwalten, ist jeder
dieser Instanzen ein dedizierter Eintity-Storage-Provider zugewiesen.
Jede World-Instanz wird über die Methoden des *Entity-Component-Managers* (ECM) verwaltet.

Das folgende `ClassDiagramms` visualsiert die beteiligten `DCES`
Elemente. Um die Darstellbarkeit zu verbessern, wird der vollständige Baum in detaillierte Unter-Bäume aufgesplittet.

```mermaid
classDiagram

World --o EntityComponentManager
World --o Resource
World --o SystemStore

EntityComponentManager --o EntityStore
EntityComponentManager --o ComponentStore

EntityComponentManager : entity_store[EntityStore]
EntityComponentManager : component_store[ComponentStore]
EntityComponentManager : component_store()
EntityComponentManager : component_store_mut()
EntityComponentManager : create_entity()
EntityComponentManager : entity_counter[u32]
EntityComponentManager : entity_store()
EntityComponentManager : entity_store_mut()
EntityComponentManager : new()
EntityComponentManager : register_entity()
EntityComponentManager : remove_entity()
EntityComponentManager : stores()
EntityComponentManager : stores_mut()

Resource: FxHashMap[TypeId, Box->dyn Any]
Resource : contains()
Resource : get()
Resource : get_mut()
Resource : insert()
Resource : is_empty()
Resource : len()
Resource : new()
Resource : try_get()
Resource : try_get_mut()

World : entity_component_manager[EntityComponetManager]
World : resources[FxHashmap]
World : system_counter[u32]
World : system_store[SystemStore]
World : first_run bool
World : create_entity()
World : create_system()
World : drop()
World : entity_component_manager()
World : from_entity_store()
World : insert_resource()
World : print_entity()
World : register_init_system()
World : resource_mut()
World : remove_entity()
World : run()
```

<span class="caption">Workflow 1-1: Globale Ansicht der `DCES` Architektur</span>

Schauen wir uns nun Teile der Einheiten Hierarchie an.

```mermaid
classDiagram

Trait_EntityStore --o VecEntityStore
Trait_EntityStore --o EntityBuilder
VecEntityStore --o Entity
EntityBuilder --o Entity
EntityBuilder --o Component

EntityBuilder : EntityStore[Entity, ComponentStore, EntityStore]
EntityBuilder : build()
EntityBuilder : components()

VecEntityStore : Vec[Entity]
VecEntityStore : register_entity()
VecEntityStore : remove_entity()

Trait_EntityStore : Entity
Trait_EntityStore : register_entity()
Trait_EntityStore : remove_entity()

Entity : Entity[u32]

Component: E[Any]
```

<span class="caption">Workflow 1-2: Detailansicht zur `Einheiten` Architekture</span>

Es folgt einer Ansicht auf die Bestandteile der Komponenten Hierarchie an.

```mermaid
classDiagram

Trait_Component --o ComponentStore
Trait_Component --o ComponentBuilder
Trait_Component --o ComponentBox
ComponentBox --o SharedComponentBox
ComponentBox --o Component
SharedComponentBox --o Component
ComponentStore --o Component
ComponentBuilder --o Component


Component : E[Any]

ComponentBuilder : ComponentBuilder[components, shared]
ComponentBuilder : build()
ComponentBuilder : new()
ComponentBuilder : with()
ComponentBuilder : with_shared()

ComponentBox : ComponentBox[Box->dyn Any, TypeId]
ComponentBox : consume()
ComponentBox : new()

ComponentStore : ComponentStore[Components, SharedComponents]
ComponentStore : append()
ComponentStore : entities_of_component()
ComponentStore : get()
ComponentStore : get_mut()
ComponentStore : is()
ComponentStore : is_empty()
ComponentStore : is_origin()
ComponentStore : len()
ComponentStore : print_entity()
ComponentStore : register()
ComponentStore : register_box()
ComponentStore : register_shared()
ComponentStore : register_shared_box()
ComponentStore : register_shared_box_by_source_key()
ComponentStore : register_shared_by_source_key()
ComponentStore : remove_entity()
ComponentStore : source()
ComponentStore : source_from_shared()
ComponentStore : target_key()

SharedComponentBox : SharedComponentBox[Entity, TypeId]
SharedComponentBox : consume()
SharedComponentBox : new()

Trait_Component : E[Any]
```

<span class="caption">Workflow 1-3: Detailansicht der `Komponenten` Architectur</span>

Gefolgt von der Darstellung der Detailansicht der System-Hierarchie.

```mermaid
classDiagram

Trait_System --o SystemStore

SystemStore --o SystemStoreBuilder
SystemStore --o EntitySystem
SystemStoreBuilder --o EntitySystem

SystemStoreBuilder <-- InitSystem
SystemStoreBuilder <-- CleanupSystem


Trait_System : EntityStore[Any]
Trait_System : run()

EntitySystem: EntitySystem[Box->dyn System, priority]
EntitySystem: new()

SystemStore : EntityStore[entity_system->HashMap, init_system>Option, cleanup_system->Option, priorities->BTreeMap]
SystemStore : borrow_cleanup_system()
SystemStore : borrow_init_system()
SystemStore : new()
SystemStore : register_cleanup_system()
SystemStore : register_init_system()
SystemStore : register_priority()
SystemStore : register_system()
SystemStore : remove_system()

SystemStoreBuilder: SystemStoreBuilder[entity_system_id, system_store, priority]
SystemStoreBuilder: build()
SystemStoreBuilder: with_priority()
```

<span class="caption">Workflow 1-4: Detailansicht der `System` Architektur</span>
