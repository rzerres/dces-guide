# The Dense Component Entity System

[<img src="img/dces_space.png" width="720"/>](img/dces_space.png)

*by Florian Blasius, with contributions from the Rust Community*<br>
*annotated and documented by Ralf Zerres and all contributors*

This version of the text assumes you’re using Rust v1.60.0 or later.
*Cargo.toml* should define `edition="2018"`. That enables and uses
Rust 2018 Edition idioms in all derived projects.

The 2020 Edition of this guide is the initial release. It will be
released with the DCES version 0.4.0.

- Appendix A Keywords, explains the new raw identifiers.
- Appendix D Translations, is work in progress. We will release
  instances of this book in the target language once they are translated.

For online reading, a HTML rendered version is available at [DCES
guide][dces_guide_en]. Alternatively you might want to have it handy
for offline usage. Either you download a rendered `pdf` or
`ebook`version or go ahead and download the source. Then kick on
mdbook (the definition of the target location is optional).

If development has stabalized, this guide will be offered in multiple
languages. The source directory is already structured to serve
[localized sub-directories][mdbook_localization]. Thus to render its
contents you need an enhanced version of
mdbook. [Ruin0x11][mdbook_branch] is maintaining a git branch
**localization**, a [PR][mdbook_pr_1306] is commited upstream.

[mdbook_localization]: https://github.com/Ruin0x11/mdBook/tree/localization
[mdbook_branch]: https://github.com/Ruin0x11/mdBook.git
[mdbook_pr_1306]: https://github.com/rust-lang/mdBook/pull/1306
[dces_guide_en]:  https://gitlab.redox-os.org/redox-os/dces-rust/wiki/guide-en/index.html

Go ahead and install that mdBook version like this:

```console
TMPDIR=<your_temporary_directory>
mkdir -p $TMPDIR; cd $TMPDIR
git clone https://github.com/Ruin0x11/mdBook.git
cd mdBook
git checkout localization
cargo update
cargo install --path .
```

We do make use of process visualization, that will need
[mermaid][mermaid_home]. To download and compile it from source,
please use the following commands:

[mermaid_home]: https://mermaid-js.github.io/mermaid/#

```console
cargo install mdbook-mermaid
mermaid install
```

Now, that all needed binaries are installed on your system you can render the guide like this:

```console
cd  <the_DCES_guide__directory>
mdbook build --language en --dest-dir dces-guide/en --open
```
