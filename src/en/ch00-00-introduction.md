# Introduction

[<img src="img/dces_logo_dark.png" width="720"/>](img/dces_space.png)

Welcome to **Dense Component Entity System**, a guide to `DCES`. In
the model of an archetype, or dense ECS the entities are stored in
tables. Components are the columns and entities are the
rows. Archetype implementations are fast to query and iterate.

This document provides a quick overview of the different features and
concepts in `DCES` with short examples. This is a good resource if
you're just getting started or just want to get a better idea of what
kind of features are available in DCES!

<div class="warning">

Warning: This guide is incomplete.  Documenting everything and
rewriting outdated parts take a while.
See the [issue tracker] to check what's missing or outdated. If there
are any mistakes or ideas that haven't been reported, feel free to
open a new issue there.

</div>

[issue tracker]: https://gitlab.redox-os.org/redox-os/dces-rust/-/issues

## Features

* Register entities with components
* Share components between entities
* Register systems and read / write components of entities
* Order systems execution by priority
* Register container for entity organization (Vec, FxHashMap, Custom Container, ...)
* Register init and cleanup system

## Who DCES is for

`DCES` does suite for programmers that like to take advantage of the
Rust programming language to develop games or enhance UI's frameworks. Since
everything is build native in Rust, there is no need to transform
data structures and types. As an example `OrbTk` will completely rely on this
entity component system (ECS) variant.

There are a number of reasons why an ECS is
gaining popularity especially amongst game developers:

	ECS can typically support larger numbers of game objects
	ECS code tends to be more reusable
	ECS code is easier to extend with new features
	ECS allows for a more dynamic coding style

## Why not just ...

You might ask, why not just use another production grade, maintained
and feature complete ECS? And you are right. There is freedom. At least there are two crates, that
are powerful and totally suitable alternative.

`DCES` primary goal has been to provide a **fast**, **dependency
free** ECS with full integration into the Orbital Toolkit
([OrbTK][orbtk]). We have thought about incorporating parallel
execution into `DCES`. This is deferred for now, since there is always
overhead in parallelization. You should carefully profile to see if
there are benefits in the switch. If you have only few things to
iterate over then sequential join is faster.

[orbtk]: https://github.com/redox-os/orbtk

### using Specs
Components and Systems in Specs may be computed in parallel. It uses as
**Dispatcher** to achieve this goal.
Details are documented inside the [Tutorial][specs_tutorial].

[specs_tutorial]: https://specs.amethyst.rs/docs/tutorials/

### using legion
The ECS demonstrated an archetypal memory layout and trait-less
components.

### using Shipyard
This is a sparse set based ECS. It stores each component in its own
sparse set, where each entity id is the key to the component. Sparse
set implementations allow for fast add/remove operations.

## Source Code

The source files from which this guide is generated can be found on
its homepage at [DCES guide (en)][dces_guide_en].

[dces_guide_en]: https://gitlab.com/redox-os/dces-rust/tree/main/guide/src/en

<!--
[dces_guide_en]: https://github.com/redox-os/dces-rust/tree/main/guide/src/en
-->
