# DCES Examples - Basic

This subsection documents the example application **dces_basic**.

This example instantiates a single **world**. It creates three
**entities** each with an assigned **component** that provides the property
"name", "depth" and "size".

A **size system** will query the entity store and manipulate the width
and height struct values of components that provide and match a key "size".
As a rudimentary use case, we do simply increment by one.

A **print system** will
queries into the entity store "e_store", collects given entities and
prints out the "value" of any component that has a matching component
with the property key "name". Without any fancy magic, entities name,
its width and its height are printed to stdout.

##  The project root

Change to your project root directory. If you didn't already create
the app in the first place, go ahead and type the following in your
console:

```console
$ cd ~/guide/examples
```

##  Create the source code

Next we will use cargo to create the app. All boilerplate tasks are
handled using cargo's inherited template handling.

```console
$ cargo new dces_basic
$ cd dces_basic
```

The first command, `cargo new`, takes the name of the project
("`dces_basic`") as the first argument. The second command changes to
the new project’s directory.

Look at the generated *Cargo.toml* file:

```toml
{{#include ./listings/ch09-01-dces-basic/no-listing-01-01-cargo-new/Cargo.toml}}
```

<span class="caption">Listing 1-1: Default metadata "dces_basic"</span>

With `cargo new`, a default project structure is created. Maybe the
author information is already exchanged if *Cargo* could obtain a
definition from your environment. *Cargo* also generated source code
for a "Hello, world!"  program. Let's Check out the corresponding
*src/main.rs* file:

```rust
{{#rustdoc_include ./listings/ch09-01-dces-basic/no-listing-01-01-cargo-new/src/main.rs}}
```

<span class="caption">Listing 1-2: Default source file "main.rs"</span>

No need to compile that stage with `cargo run`, since we are going to
exchange the project metadata, as well as the dces source code right
away.

### Update Cargo.toml

First reopen the *Cargo.toml* file and enter the Code in Listing 1-3 into *Cargo.toml*

```toml,ignore
{{#include ./listings/ch09-01-dces-basic/listing-01-01/Cargo.toml:All}}
```

<span class="caption">Listing 1-3: Project metadata "dces_basic"</span>

You may wonder, why the *name* property inside the *Cargo.toml* is
formatted like `dces_basic`.

```toml,ignore
{{#include ./listings/ch09-01-dces-basic/listing-01-01/Cargo.toml:Name}}
```

It is a good habit to follow rusts naming convention, that encourages
you to use [snake_case][naming] naming. While expanding the **DCES**
example sources, we will keep the grouping prefix `dces`. That way we
end up to call our first target binary `dces_basic`.

[naming]: https://rust-lang.github.io/api-guidelines/naming.html

### Update main.rs

All of the **DCES** specific code that is needed to build our first
example is shown in [Listing 1-4][dces_basic], that you encode into
*src/main.rs*.

Save your changes to the file and go back to the terminal
window. Enter the following commands to compile and run the file in
debug mode:

```console
$ cargo run --example basic
```

The following output should be printed inside your console window:

```console
Button width: 6; height: 6
CheckBox width: 4; height: 4
RadioButton width: 5; height: 7
```

[dces_basic]: #complete-example-source

## Recap and annotation

### The anatomy of a DCES application

Let’s review in detail what just happened in your **DCES-basic** application.
Here are the relevant pieces of the puzzle:

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:Use}}
```

The first line is introducing a *use* declaration. A *use* declaration is used
to shorten the path required to refer to rust module items. The *prelude* is a
convenient way to a list of things, that rust will automatically import to you
program. Here, we bind the path *dces::prelude*. All default items defined in
this path (referenced with *::*) are now accessible in your source using their
shorthand name. No need to type in their common prefix (*dces::prelude::*)

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:FunctionMain}}
```

The `main` function is special: it defines the first execution point inside
a compiled Rust binary (leaving rust's minimal
 runtime out of scope). In our example the function name
 `main` has no parameters and returns nothing. If there were
 parameters, they would go inside the parentheses, `()`.

Also, note that the function body is wrapped in curly brackets, `{}`. Rust
requires these around all function bodies. It’s good style to place the opening
curly bracket on the same line as the function declaration, adding one space in
between.

An automatic formatter tool called `rustfmt` will help you to stick to a
standard style across Rust projects. DCES is following this guidance.
`rustfmt` will format your code in a particular style. Depending on the version
of your rust toolchain, it is probably already installed on your computer!
Check the online documentation for more details.

Lets focus on some other important details:

* First, Rust style is to indent with four spaces, not a tab.
* Second, we create five **entities** inside a **world**.
* Third, we create two **systems** inside a world.
* Forth, we start the **run loop** for our world.

As a first step, we do instantiate a mutable world structure. This
structure will make use of an **EnityStore**.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:CreateWorld}}
```

Next we create five new **entities** inside the our world. For each
entity we assign **components**. The components are constructed
consuming the method **ComponentBuilder** to define their values
(uppercase string). The corresponding keys are **name**, **depth**,
and **size** (in parentheses).  A terminating block is calling the
build method, that finally instantiates the code.

Following code block extracts the definition of the first entity:

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:CreateEntity}}
```
Please note, that "size" itself is a structure that handles the values **width** and
**height**.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:StructSize}}
```

We should mention, that our example-code defines all entities with the
same number and names of component elements. This is not a requirement
nor a design restriction. Which components are attached to which
entities may differ significantly and is completely arbitrary.

Our binary will consume a **Size-** and a **Print-System**. Both of
them do handle entity manipulation via the **EntityComponentManager
(ECM)**. The ECM is capable to borrow or mutate Enities and Components
via its **EntityStore**. Thus we can assign distinct **store
structures** for each of them (here: e_store, c_store).


```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:Implement_SizeSystem}}
```

Next we simply loop over the store in question and use the
**get()** method to consume the values of the given entities and
components that match our query.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:Loop_Entities}}
```

### Complete example source

Find attached the complete source code for our **dces_basic** example.

```rust,ignore
{{#rustdoc_include ./listings/ch09-01-dces-basic/listing-01-01/src/main.rs:All}}
```

<span class="caption">Listing 1-4: dces_basic - Create a World, its entities and systems.</span>

### Compiling and Running Are Separate Steps

Before you are able to run a DCES application, you must compile its
source code. A typical DCES project will generate the executable
binary code using cargo and place the result in the target subfolder
of the project.

Profiles may be used to configure compiler options such as optimization levels
and debug settings. By default the `dev` or `test` profiles are used. If the
`--release` flag is given, then the release or bench profiles are used.

```console
$ cargo build --release --bin dces_basic.rs
$ ../target/release/dces_basic
```

On Windows, you need to use `backslash` as a path delimiter:

```powershell
> cargo build --release --bin dces_basic.rs
> ..\target\release\dces_basic.exe
```

[troubleshooting]: https://doc.redox-os.org/dces-guide/ch01-01-installation.html#troubleshooting
