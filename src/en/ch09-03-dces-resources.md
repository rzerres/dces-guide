# DCES Examples - Resource

This subsection documents the example application **dces_resource**.

Again, we create a single **world** instance. Next one **entity** is
build and we assignes one **component** that provides the property
"name". A mutable **resource** consuming type "HelloDCES" is assigned
as well. Inside the **print system** we take care to print out any
property value that matches the component key "name". In addition the
"say_hello()" method of resource "HelloDCES" is executed, that simply
returns the **String** "Hello DCES!".

## Update Cargo.toml

First have a look at the corresponding *Cargo.toml* file as shown in Listing 1-1.

```toml,ignore
{{#include ./listings/ch09-03-dces-resources/listing-03-01/Cargo.toml}}
```

<span class="caption">Listing 1-1: Projects metadata "dces_resources"</span>

## Program source code

All of the **DCES** specific code that is needed to build the **dces_resources**
example is shown in [Listing 1-2][dces_resource], that is encode in
*src/main.rs*.

Enter the following commands inside the terminal window to compile and
run in debug mode:

```console
$ cargo run --example resource
```

The following output should be printed inside your console window:

```console
DCES
Hello DCES!
```

[dces_resource]: #complete-example-source

## Recap and annotation

### The anatomy of the `dces_resource` application

Let’s review the relevant parts of the **dces_resource** application.
A more in depth view of a typical `DCES` application is documented
inside the annotated [dces_basic][dces_basic_source] source.

[dces_basic_source]: ./ch09-01-dces-basic.md

Following code block extracts the implementation of the PrintSystem:

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:Implement_PrintSystem}}
```

The value of component key `"name"` will be processed as already
documented. The new part is inside the handling of the
Resource-Structure via the PrintSystem.

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:StructResource}}
```

The code implements a the **HelloDCES** type which provides the
**say_hello()** method.

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:Implement_Resource}}
```

With the resource `get()` method, we chain the call to its
`say_hello()` method. It returns a **String** that is
passed over to the **println!()** macro.

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:Print_StructResource}}
```

As a result, the string `Hello DCES!` will make it as the console output.

### Complete example source

Find attached the complete source code for our **dces_resource** example.

```rust,ignore
{{#rustdoc_include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:All}}
```

<span class="caption">Listing 1-2: dces_resource - Create a World, its entities, resources and a print systems.</span>

### Compiling and Running Are Separate Steps

The compiled `dces_resource` with **cargo** will be placed the
resulting binary in the target subfolder of the project.

```console
$ cargo build --release --bin dces_resource.rs
$ ../target/release/dces_resource
```

On Windows, you need to use `backslash` as a path delimiter:

```powershell
> cargo build --release --bin dces_resource.rs
> ..\target\release\dces_resource.exe
```
