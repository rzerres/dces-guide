# DCES Examples - Shared

This subsection documents the example application **dces_shared**.

In a minimalistic use-case, this example creates a single
**world**. It creates two **entities**. Each entity gets assinged
**components** that provide the properties “name”, “depth” and “size”.
When using the **ComponentBuilder** for the second entity (component
"name" == "CheckBox") we make use of a method "with_shared()", that
will reference to an already given struct "size".

A size system will query the entity store and manipulate the width and
height struct values of components that provide and match a key
“size”. As a rudimentary use case, we do simply increment them by one.

A print **system** will queries into the entity store “e_store”, collects
given entities and prints out the “value” of any component that has a
matching property key “name”. Without any fancy
magic, entities name, its width and its height are printed to stdout.
and assignes three **component** that provides the property "name",
"depth" and "size".


## Update Cargo.toml

First have a look at the corresponding *Cargo.toml* file as shown in Listing 1-1.

```toml,ignore
{{#include ./listings/ch09-04-dces-shared/listing-04-01/Cargo.toml}}
```

<span class="caption">Listing 1-1: Projects metadata "dces_shared"</span>

## Program source code

All of the **DCES** specific code that is needed to build the **dces_shared**
example is shown in [Listing 1-2][dces_shared], that is encode in
*src/main.rs*.

Enter the following commands inside the terminal window to compile and
run in debug mode:

```console
$ cargo run --example shared
```

The following output should be printed inside your console window:

```console
entity: 0; name: Button; width: 6; height: 6
entity: 1; name: CheckBox; width: 6; height: 6
```

[dces_shared]: #complete-example-source

## Recap and annotation

### The anatomy of the `dces_shared` application

Let’s review the relevant parts of the **dces_shared** application.
A more in depth view of a typical `DCES` application is documented
inside the annotated [dces_basic][dces_basic_source] source.

[dces_basic_source]: ./ch09-01-dces-basic.md

Following code block extracts the implementation of the PrintSystem:

```console
{{#include ./listings/ch09-04-dces-shared/listing-04-01/src/main.rs:Implement_PrintSystem}}
```

The values of the component keys `"name"`, `"width"` and `"height"`
will be processed as already documented.

The new part is inside the handling of the Size-Structure to a
component. Inside the *main* function, when we do attach the component
"size" with **ComponentBuilder** inside the second entity, we make use
of the `with_shared` method. This method is able to borrow the already
assigned structure "size". Therefore it will reference the given values.

```console
{{#include ./listings/ch09-04-dces-shared/listing-04-01/src/main.rs:ComponentShared}}
```

### Complete example source

Find attached the complete source code for our **dces_shared** example.

```rust,ignore
{{#rustdoc_include ./listings/ch09-04-dces-shared/listing-04-01/src/main.rs:All}}
```

<span class="caption">Listing 1-2: dces_shared - Create a World, its entities and a shared component.</span>

### Compiling and Running Are Separate Steps

The compiled `dces_shared` with **cargo** will be placed the resulting
binary in the target subfolder of the project.

```console
$ cargo build --release --bin dces_shared.rs
$ ../target/release/dces_shared
```

On Windows, you need to use `backslash` as a path delimiter:

```powershell
> cargo build --release --bin dces_shared.rs
> ..\target\release\dces_shared.exe
```
