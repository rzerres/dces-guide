// ANCHOR: All
// ANCHOR: Use
use dces::prelude::*;
// ANCHOR_END: Use

// ANCHOR: StructSize
#[derive(Default)]
struct Size {
    width: u32,
    height: u32,
}
// ANCHOR_END: StructSize

#[derive(Default)]
struct Name(String);

#[derive(Default)]
struct Depth(u32);

pub struct SizeSystem;
// ANCHOR: Implement_SizeSystem
impl System<EntityStore> for SizeSystem {
    // ANCHOR: Run_EntityStore
    fn run(&self, ecm: &mut EntityComponentManager<EntityStore>, _: &mut Resources) {
        let (e_store, c_store) = ecm.stores_mut();

        // ANCHOR: Loop_Entities
        for entity in &e_store.inner {
            if let Ok(comp) = c_store.get_mut::<Size>("size", *entity) {
                comp.width += 1;
                comp.height += 1;
            }
        }
        // ANCHOR_END: Loop_Entities
    }
    // ANCHOR_END: Run_EntityStore
}
// ANCHOR_END: Implement_SizeSystem

pub struct PrintSystem;
impl System<EntityStore> for PrintSystem {
    fn run(&self, ecm: &mut EntityComponentManager<EntityStore>, _: &mut Resources) {
        let (e_store, c_store) = ecm.stores_mut();

        for entity in &e_store.inner {
            if let Ok(name) = c_store.get::<Name>("name", *entity) {
                if let Ok(size) = c_store.get::<Size>("size", *entity) {
                    println!("{} width: {}; height: {}", name.0, size.width, size.height);
                }
            }
        }
    }
}

// ANCHOR: Main
// ANCHOR: FunctionMain
fn main() {
    // ANCHOR_END: FunctionMain
    // ANCHOR: CreateWorld
    let mut world = World::from_entity_store(EntityStore::default());
    // ANCHOR_END: CreateWorld

    // ANCHOR: CreateEntity
    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with("name", Name(String::from("Button")))
                .with("depth", Depth(4))
                .with(
                    "size",
                    Size {
                        width: 5,
                        height: 5,
                    },
                )
            // ANCHOR: MethodBuild
                .build(),
            // ANCHOR_END: MethodBuild
        )
        .build();
    // ANCHOR_END: CreateEntity

    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with("name", Name(String::from("CheckBox")))
                .with("depth", Depth(1))
                .with(
                    "size",
                    Size {
                        width: 3,
                        height: 3,
                    },
                )
                .build(),
        )
        .build();

    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with("name", Name(String::from("RadioButton")))
                .with("detph", Depth(2))
                .with(
                    "size",
                    Size {
                        width: 4,
                        height: 6,
                    },
                )
                .build(),
        )
        .build();

    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with("depth", Depth(3))
                .with(
                    "size",
                    Size {
                        width: 10,
                        height: 4,
                    },
                )
                .build(),
        )
        .build();

    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with("depth", Depth(0))
                .with(
                    "size",
                    Size {
                        width: 5,
                        height: 8,
                    },
                )
                .build(),
        )
        .build();

    // ANCHOR: System
    world.create_system(PrintSystem).with_priority(1).build();

    world.create_system(SizeSystem).with_priority(0).build();
    // ANCHOR_END: System

    // ANCHOR: Run
    world.run();
    // ANCHOR_END: Run
}
// ANCHOR_END: Main
// ANCHOR_END: All
