# DCES Guide
![DCES Guide](src/img/dces_space.png "DCES Guide")

[![Build status](https://gitlab.redox-os.org/redox-os/dces-rust/badges/develop/pipeline.svg)](https://gitlab.redox-os.org/redox-os/dces-rust/pipelines)
[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)
[![Latest Release](https://gitlab.redox-os.org/redox-os/dces-guide/-/badges/release.svg)](https://gitlab.redox-os.org/redox-os/dces-guide/-/releases)
<!-- [![CI-Tests](https://gitlab.com/redox-os/dces-guide/workflows/CI/badge.svg)](https://gitlab.com/redox-os/dces-guide/actions) -->

DCES is a library that provides a variant of the Entity Component
System [`ECS`][ecs].

The goal of DCES is a lightweight ECS library with zero dependencies.
This eases creation of UI frameworks and game engines. It is being
developed as part of OrbTk, a (G)UI framework written in Rust. All
widgets and properties of OrbTk are handled by DCES.

This guide will dig into the concept root of DCES, and provided more
insides. It discusses the very concept on which `DCES` is built. A
bird`s eye view of essential building blocks is presented. Their
discussion is co-centered on the details relevant to the
implementation.

This guide is provided as a sub-module of `DCES`.

[ecs]: https://en.wikipedia.org/wiki/Entity–component–system.

## Features

* Register entities with components
* Share components between entities
* Register systems and read / write components of entities
* Order systems execution by priority
* Register container for entity organization (Vec, FxHashMap, Custom Container, ...)
* Register init and cleanup system

## Planned features

* Concurrency of systems with same priority
* Advanced example

## Documentation

If development has stabalized, this guide will be offered in multiple
languages. The source directory is already structured to serve
localized sub-directories consuming `mdbook`.

### Online Guide

The following link will provide an online accessible version of the
[english variant][guide_en] of the guide:

`https://doc.redox-os.org/dces-guide`

[guide_en]: https://doc.redox-os.org/dces-guide

A CI/CD pipeline will render an updated version, as soon as enhanced
content is merged into the `main` branch.

### Offline Guide

To render the source contents of the guide, you need an enhanced
version of mdbook. Ruin0x11 is maintaining a git branch `localization`,
and a PR is already commited upstream.

Go ahead and install that mdBook version like this:

```
TMPDIR=<your_temporary_directory>
mkdir -p $TMPDIR; cd $TMPDIR
git clone https://github.com/Ruin0x11/mdBook.git
cd mdBook
git checkout localization
cargo update
cargo install --path .
```

We do make use of process visualization, that will need mermaid. To
download and compile it from source, please use the following
commands:

```
cargo install mdbook-mermaid
mermaid install
```

Now, that all needed binaries are installed on your system you can render the guide like this:

```
cd  <the_DCES_guide__directory>
mdbook build --language en --dest-dir dces-guide/en --open
```

## Inspirations

* [Rustic Entity-Component System](https://github.com/AndyBarron/rustic-ecs)
* [Specs - Parallel ECS](https://github.com/slide-rs/specs)
* [Shipyard](https://github.com/leudz/shipyard)

## License

<!-- License source -->
[Logo-CC_BY]: https://i.creativecommons.org/l/by/4.0/88x31.png "Creative Common Logo"
[License-CC_BY]: https://creativecommons.org/licenses/by/4.0/legalcode "Creative Common License"

This work is licensed under a [Creative Common License 4.0][License-CC_BY]

![Creative Common Logo][Logo-CC_BY]

© 2022 Ralf Zerres
